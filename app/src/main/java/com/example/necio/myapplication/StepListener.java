package com.example.necio.myapplication;

public interface StepListener {

    public void step(long timeNs);
}
