package com.example.necio.myapplication;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import mqtt.AtuadorGenerico;
import mqtt.ClientMQTT;
import mqtt.SensorGenerico;

public class MainActivity extends AppCompatActivity implements SensorEventListener, StepListener{

    ClientMQTT mqtt;
    Button btnUmidade, btnTemperatura, btnMovimento, btnLuminosidade;
    Switch swLampada, swAr, swTV;
    EditText ipGateway;

    private StepDetector simpleStepDetector;
    private SensorManager sensorManager;
    private Sensor accel;
    private static final String TEXT_NUM_STEPS = "Número de passos: ";
    private int numSteps;

    private TextView textView;
    AtuadorGenerico passos;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ipGateway      = (EditText) findViewById(R.id.editText);
        Button start = (Button) findViewById(R.id.button5);


        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        accel = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        simpleStepDetector = new StepDetector();
        simpleStepDetector.registerListener(this);

        textView = (TextView) findViewById(R.id.textView);
        numSteps = 0;
        sensorManager.registerListener(MainActivity.this, accel, SensorManager.SENSOR_DELAY_FASTEST);

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                passos = new AtuadorGenerico(getApplicationContext(), ipGateway.getText().toString(), "passos");
            }
        });


        //startMqtt();

    }

    private void startMqtt() {
        mqtt = new ClientMQTT(getApplicationContext());
        mqtt.setCallback(new MqttCallbackExtended() {
            @Override
            public void connectComplete(boolean b, String s) {

            }

            @Override
            public void connectionLost(Throwable throwable) {

            }

            @Override
            public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {
                Log.w("Debug", mqttMessage.toString());
                //dataReceived.setText(mqttMessage.toString());
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

            }
        });
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            simpleStepDetector.updateAccel(
                    event.timestamp, event.values[0], event.values[1], event.values[2]);
        }
    }

    @Override
    public void step(long timeNs) {
        numSteps++;
        textView.setText(TEXT_NUM_STEPS + numSteps);
        //Log.w("passos", Integer.toString(numSteps));
        if (passos != null)
            passos.publicar(numSteps);
        else
            passos = new AtuadorGenerico(getApplicationContext(), ipGateway.getText().toString(), "passos");

    }
}
